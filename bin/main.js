const { app, BrowserWindow, Menu, Tray } = require('electron')

// 创建浏览器窗口

//app.whenReady().then(createWindow)
function openwin(wind) {
  wind.show()
  wind.setSkipTaskbar(false)
}
let tray = null
app.on('ready', () => {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    frame: false,
    icon: './static/logo.png',
    webPreferences: {
      nodeIntegration: true,
    },
  })
  win.on('close', (event) => {
    win.hide()
    win.setSkipTaskbar(true)
    event.preventDefault()
  })

  win.loadFile('bin/index.html')
  // 打开开发者工具
  Menu.setApplicationMenu(null)
  //win.webContents.openDevTools()
  tray = new Tray('./static/logo.png')
  const contextMenu = Menu.buildFromTemplate([
    { label: '显示主界面', type: 'radio', click: () => openwin(win) },
    {
      label: '退出',
      type: 'radio',
      click: () => {
        win.destroy()
        app.quit()
      },
    },
  ])
  tray.setToolTip('这是用来加载edgeless插件的玩意')
  tray.setContextMenu(contextMenu)
})
//app.on('window-all-closed', () => {
//  if (process.platform !== 'darwin') {
//    app.quit()
//  }
//})

//app.on('activate', () => {
//  if (BrowserWindow.getAllWindows().length === 0) {
//    createWindow()
//  }
//})
